package datastructure;

import cellular.CellState;

import java.awt.*;
import java.util.ArrayList;

public class CellGrid implements IGrid {

   private int rows;
   private int columns;
   private CellState initialState;
   private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i ++){
            for (int j = 0; j < columns; j++){
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    private boolean outOfBounce(int rows, int columns){
        if(rows >= 0 && rows < numRows() || columns >= 0 && columns < numColumns()){
            return false;
        }
        return true;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (outOfBounce(row, column))
            throw new IllegalArgumentException("Invalid argument");
        grid[row][column] = element;

    }

    @Override
    public CellState get(int row, int column) {
        if (outOfBounce(row,column))
            throw new IllegalArgumentException("Invalid argument");
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copiedGrid = new CellGrid(rows,columns,null);
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j ++){
                copiedGrid.set(i,j,get(i,j));
            }
        }
        return copiedGrid;
    }
    
}
